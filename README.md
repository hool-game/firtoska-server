Install Redis on Linux, macOS, and Windows
  https://redis.io/docs/getting-started/installation/

After installing redis, we have to add a json plugin for Redis
  download the plugin from this link
  https://upload.disroot.org/r/HNNc1yve#IgwZdPpCocv6i4Y3tqgswpyqXZexMBlq+NuFZ+Li9Cw=

  download the librejson.so from the link and move the file to /etc/redis (filePath) for linux

  open the redis.conf file (/etc/redis/redis.conf)
  add the module under ####MODULE####
    loadmodule /etc/redis/librejson.so 

