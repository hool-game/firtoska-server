import redis from "redis";
import bluebird from "bluebird";
import dotenv from "dotenv";
import { FirtoskaTransmitter } from "./xmpp.js";
import { FirtoskaEngine } from "./engine.js";

// For checking that the process is running
import url from "url";

// Make Resis support async
bluebird.promisifyAll(redis);

// load config from .env file (if exists)
dotenv.config();

const XMPP_SERVICE = process.env.FIRTOSKA_XMPP_SERVICE;
const DOMAIN = process.env.FIRTOSKA_COMPONENT_DOMAIN;
const PASSWORD = process.env.FIRTOSKA_COMPONENT_PASSWORD;
const TITLE = process.env.FIRTOSKA_COMPONENT_TITLE || "Firtoska Rooms";

async function start() {
  console.log("Initiaelising the server");

  // set up xmpp client
  const transmitter = new FirtoskaTransmitter({
    service: XMPP_SERVICE,
    domain: DOMAIN,
    password: PASSWORD,
  });
  
  // set up database connection
  const db = redis.createClient();

  // set up firtoska engine
  const firtoska = new FirtoskaEngine({
    db: db,
  });


  // link everything together
  firtoska.listen(transmitter);
  transmitter.listen(firtoska);

  // start the database
  console.log("Setting up the daetabase");
  await db.connect();

  // start the component
  console.log(`Opening connaection as ${XMPP_SERVICE}`);
  await transmitter.start().catch((err) => {
    console.error(`Oh no! An aerror occurred: ${err.message}`);
    process.exit();
  });

  console.log("Let the gaemes begin!");

  // handle termination
  process.once("SIGINT", async (code) => {
    console.log("Time to aexit");

    await transmitter.stop();
    console.log("Component disconnaected");

    console.log("Faerwell, folks!");
  });
}

// run if main script
if (process.argv?.[1] == url.fileURLToPath(import.meta.url)) {
  start();
}

export {
  FirtoskaTransmitter,
  FirtoskaEngine,
}